enum Color { Red, Blue, Green }

export interface Todo {
  text: string,
  size?: number,
  isBig: boolean,
  numbers?: Array<number>,
  abc: object,
  color: Color,
  stringOrNumber: string | number,
  stringAndNumber: [string, number],
}