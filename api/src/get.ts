import { client } from './dynamodb';

export const get = async (event) => {
  const params = {
    TableName: process.env.DYNAMODB_TABLE || '',
    Key: {
      id: event.pathParameters.id,
    },
  };
  try {
    const result = await client.get(params).promise(); 
    return {
      statusCode: 200,
      body: JSON.stringify(result.Item),
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: error.statusCode || 501,
      headers: { 'Content-Type': 'text/plain' },
      body: 'Couldn\'t fetch the todo item.',
    };
  }
};
