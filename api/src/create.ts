import { v1 } from 'uuid';
import { client } from './dynamodb';
// equals or is
import { is, equals } from 'typescript-is';
import { Todo } from './todo';

/*
curl --request POST \
  --url http://localhost:3000/dev/todos/ \
  --header 'content-type: application/json' \
  --data '{
	"text": "abc",
	"site": 3,
	"color": 1,
	"isBig": true,
	"abc": {}
}'
*/

export const create = async (event) => {
  const timestamp = new Date().getTime();
  const data: Todo = JSON.parse(event.body);
  if (is<Todo>(data)) {
    const params = {
      TableName: process.env.DYNAMODB_TABLE || '',
      Item: {
        id: v1(),
        ...data,
        checked: false,
        createdAt: timestamp,
        updatedAt: timestamp,
      },
    };
    try {
      await client.put(params).promise();
      return {
        statusCode: 200,
        body: JSON.stringify(params.Item),
      }
    } catch (error) {
      console.error(error);
      return {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain' },
        body: 'Couldn\'t create the todo item.',
      };
    }
  } else {
    console.error('Validation Failed');
    return {
      statusCode: 400,
      headers: { 'Content-Type': 'text/plain' },
      body: 'Couldn\'t create the todo item.',
    };
  }
};
