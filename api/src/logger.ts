export const triggerStream = (event, context, callback) => {
  console.log(`The following happend in the DynamoDB database table "users":\n${JSON.stringify(event.Records[0].dynamodb, null, 2)}`);
  console.log(event.Records[0]);
  callback(null, { event });
};